﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Configuration;
using System.Net;
using System.IO;
using Dapper;
using System.Data.SqlClient;
using System.Data;

namespace GISCommissionsRunner
{
  public partial class Form1 : Form
  {
    private class Network
    {
      public string NetworkName { get; set; }
      public int NetworkID { get; set; }
      public string NetPref { get; set; }


    }
    private Dictionary<int, string> monthChoices;
    public Form1()
    {
      InitializeComponent();
      txtExportLoc.Text = ConfigurationManager.AppSettings["ExportLoc"];

      monthChoices = new Dictionary<int, string>();
      monthChoices.Add(1, "January");
      monthChoices.Add(2, "February");
      monthChoices.Add(3, "March");
      monthChoices.Add(4, "April");
      monthChoices.Add(5, "May");
      monthChoices.Add(6, "June");
      monthChoices.Add(7, "July");
      monthChoices.Add(8, "August");
      monthChoices.Add(9, "September");
      monthChoices.Add(10, "October");
      monthChoices.Add(11, "November");
      monthChoices.Add(12, "December");
      cmbMonth.DataSource = new BindingSource(monthChoices, null);
      cmbMonth.DisplayMember = "Value";
      cmbMonth.ValueMember = "Key";

      numYear.Maximum = DateTime.Now.Year;
      numYear.Minimum = DateTime.Now.Year - 1;

      if (DateTime.Now.Month == 1) {
        cmbMonth.SelectedValue = 12;
        numYear.Value = numYear.Minimum;
      } else {
        cmbMonth.SelectedValue = DateTime.Now.Month - 1;
        numYear.Value = numYear.Maximum;
      }

#if DEBUG
      txtRSPW.Text = "";
#endif    
    }

    private void button1_Click(object sender, EventArgs e)
    {
      using (var fbd = new FolderBrowserDialog()) {
        fbd.SelectedPath = txtExportLoc.Text;
        DialogResult result = fbd.ShowDialog();
        if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath)) {
          txtExportLoc.Text = fbd.SelectedPath;
          Configuration config = ConfigurationManager.OpenExeConfiguration(Application.ExecutablePath);

          config.AppSettings.Settings.Remove("ExportLoc");
          config.AppSettings.Settings.Add("ExportLoc", fbd.SelectedPath);

          config.Save(ConfigurationSaveMode.Modified);
        }
      }
    }

    private void btnGo_Click(object sender, EventArgs e)
    {
      if (String.IsNullOrEmpty(txtExportLoc.Text)) {
        txtExportLoc.Focus();
        return;
      }

      DirectoryInfo di = new DirectoryInfo(txtExportLoc.Text);

      string mVal = cmbMonth.SelectedValue.ToString();
      string yVal = numYear.Value.ToString();

      string monthName = monthChoices[int.Parse(cmbMonth.SelectedValue.ToString())];
      string path = "";

      string sd = "01/" + ("0" + mVal).Substring(("0" + mVal).Length-2) + "/" + yVal;
      DateTime sdt = DateTime.Parse(sd);
      DateTime edt = sdt.AddMonths(1);
      edt = edt.AddSeconds(-1);

      string ds = $"Data Source=172.16.23.204\\SECURAH;initial catalog=TPAFAdmin;integrated security=False;MultipleActiveResultSets=True;user=sa;password={txtSAPW.Text}";
      IEnumerable<Network> networks = null;
      using (var conn = new SqlConnection(ds)) {
        conn.Open();
        DynamicParameters parameter = new DynamicParameters();
        parameter.Add("@sd", sdt, DbType.DateTime, ParameterDirection.Input);
        parameter.Add("@ed", edt, DbType.DateTime, ParameterDirection.Input);
        var sql = "SELECT DISTINCT [NetworkID], [NetworkName], [NetPref] FROM [dbo].[GISSalesInfo] (@sd, @ed,0) WHERE [CommissionRate] > 0 order by 1";

        networks = conn.Query<Network>(sql, parameter);
      }

        using (WebClient client = new WebClient()) {
        client.UseDefaultCredentials = false;
        client.BaseAddress = "http://172.16.23.204";
        client.Credentials = new NetworkCredential(txtRSUsername.Text,txtRSPW.Text);
        try {
          path = Path.Combine(di.FullName, "Matrix Raw " + monthName.Substring(0, 3) + " " + numYear.Value.ToString() + ".xls");
          string url = "/ReportServer$SQL2005/?%2fGISecure%2fReports%2fGIS+Sales+Distributions+Matrix&rs:Command=Render&rs:Format=Excel&ReportMonth=" + mVal + "&ReportYear=" + yVal;
          client.DownloadFile(url, path);
        }
        catch (Exception ex) {
          MessageBox.Show("Unable to download sales distributions matrix because " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
          return;
        }
       
        foreach (Network net in networks) {
          try {
            path = Path.Combine(di.FullName, net.NetworkName);

            DirectoryInfo di2 = new DirectoryInfo(path);
            if (!di2.Exists) {
              try {
                Directory.CreateDirectory(path);
              } catch (Exception ex) {
                MessageBox.Show($"Unable to create directory {path} because " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
              }
            }

            path = Path.Combine(path, net.NetPref + " " + monthName.Substring(0, 3) + " " + numYear.Value.ToString() + ".xls");
            string url = "/ReportServer$SQL2005/?%2fGISecure%2fReports%2fGIS+Broker+Commission+Statement&rs:Format=Excel&rs:command=Render&Network=" + net.NetworkID.ToString() + "&ReportMonth=" + mVal + "&ReportYear=" + yVal;
            client.DownloadFile(url, path);
          }
          catch (Exception ex) {
            MessageBox.Show($"Unable to download broker commission statement for {net.NetworkName} because " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
          }

        }
        MessageBox.Show("Finished", "Finished", MessageBoxButtons.OK, MessageBoxIcon.Information);
      }
    }

  }
}

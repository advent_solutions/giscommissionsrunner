﻿namespace GISCommissionsRunner
{
  partial class Form1
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null)) {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.button1 = new System.Windows.Forms.Button();
      this.txtExportLoc = new System.Windows.Forms.TextBox();
      this.label1 = new System.Windows.Forms.Label();
      this.cmbMonth = new System.Windows.Forms.ComboBox();
      this.label2 = new System.Windows.Forms.Label();
      this.numYear = new System.Windows.Forms.NumericUpDown();
      this.btnGo = new System.Windows.Forms.Button();
      this.txtRSUsername = new System.Windows.Forms.TextBox();
      this.label9 = new System.Windows.Forms.Label();
      this.txtRSPW = new System.Windows.Forms.TextBox();
      this.label8 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.txtSAPW = new System.Windows.Forms.TextBox();
      ((System.ComponentModel.ISupportInitialize)(this.numYear)).BeginInit();
      this.SuspendLayout();
      // 
      // button1
      // 
      this.button1.Location = new System.Drawing.Point(12, 12);
      this.button1.Name = "button1";
      this.button1.Size = new System.Drawing.Size(93, 23);
      this.button1.TabIndex = 1;
      this.button1.Text = "Export Location";
      this.button1.UseVisualStyleBackColor = true;
      this.button1.Click += new System.EventHandler(this.button1_Click);
      // 
      // txtExportLoc
      // 
      this.txtExportLoc.Location = new System.Drawing.Point(150, 15);
      this.txtExportLoc.Name = "txtExportLoc";
      this.txtExportLoc.Size = new System.Drawing.Size(285, 20);
      this.txtExportLoc.TabIndex = 2;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(13, 53);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(37, 13);
      this.label1.TabIndex = 9;
      this.label1.Text = "Month";
      // 
      // cmbMonth
      // 
      this.cmbMonth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cmbMonth.FormattingEnabled = true;
      this.cmbMonth.Location = new System.Drawing.Point(150, 50);
      this.cmbMonth.MaxDropDownItems = 12;
      this.cmbMonth.Name = "cmbMonth";
      this.cmbMonth.Size = new System.Drawing.Size(121, 21);
      this.cmbMonth.TabIndex = 3;
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(13, 88);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(29, 13);
      this.label2.TabIndex = 11;
      this.label2.Text = "Year";
      // 
      // numYear
      // 
      this.numYear.Location = new System.Drawing.Point(150, 86);
      this.numYear.Name = "numYear";
      this.numYear.Size = new System.Drawing.Size(59, 20);
      this.numYear.TabIndex = 4;
      // 
      // btnGo
      // 
      this.btnGo.Location = new System.Drawing.Point(360, 237);
      this.btnGo.Name = "btnGo";
      this.btnGo.Size = new System.Drawing.Size(75, 23);
      this.btnGo.TabIndex = 8;
      this.btnGo.Text = "Go";
      this.btnGo.UseVisualStyleBackColor = true;
      this.btnGo.Click += new System.EventHandler(this.btnGo_Click);
      // 
      // txtRSUsername
      // 
      this.txtRSUsername.Location = new System.Drawing.Point(150, 121);
      this.txtRSUsername.Name = "txtRSUsername";
      this.txtRSUsername.Size = new System.Drawing.Size(171, 20);
      this.txtRSUsername.TabIndex = 5;
      this.txtRSUsername.Text = "emily.graham@advent.global";
      // 
      // label9
      // 
      this.label9.AutoSize = true;
      this.label9.Location = new System.Drawing.Point(13, 159);
      this.label9.Name = "label9";
      this.label9.Size = new System.Drawing.Size(122, 13);
      this.label9.TabIndex = 24;
      this.label9.Text = "Report Server Password";
      // 
      // txtRSPW
      // 
      this.txtRSPW.Location = new System.Drawing.Point(150, 156);
      this.txtRSPW.Name = "txtRSPW";
      this.txtRSPW.Size = new System.Drawing.Size(100, 20);
      this.txtRSPW.TabIndex = 6;
      this.txtRSPW.UseSystemPasswordChar = true;
      // 
      // label8
      // 
      this.label8.AutoSize = true;
      this.label8.Location = new System.Drawing.Point(13, 124);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size(124, 13);
      this.label8.TabIndex = 25;
      this.label8.Text = "Report Server Username";
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(16, 198);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(42, 13);
      this.label3.TabIndex = 26;
      this.label3.Text = "SA PW";
      // 
      // txtSAPW
      // 
      this.txtSAPW.Location = new System.Drawing.Point(150, 195);
      this.txtSAPW.Name = "txtSAPW";
      this.txtSAPW.Size = new System.Drawing.Size(100, 20);
      this.txtSAPW.TabIndex = 7;
      this.txtSAPW.UseSystemPasswordChar = true;
      // 
      // Form1
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(449, 271);
      this.Controls.Add(this.txtSAPW);
      this.Controls.Add(this.label3);
      this.Controls.Add(this.label8);
      this.Controls.Add(this.label9);
      this.Controls.Add(this.txtRSPW);
      this.Controls.Add(this.txtRSUsername);
      this.Controls.Add(this.btnGo);
      this.Controls.Add(this.numYear);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.cmbMonth);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.txtExportLoc);
      this.Controls.Add(this.button1);
      this.Name = "Form1";
      this.Text = "GIS Commissions Runner";
      ((System.ComponentModel.ISupportInitialize)(this.numYear)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Button button1;
    private System.Windows.Forms.TextBox txtExportLoc;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.ComboBox cmbMonth;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.NumericUpDown numYear;
    private System.Windows.Forms.Button btnGo;
    private System.Windows.Forms.TextBox txtRSUsername;
    private System.Windows.Forms.Label label9;
    private System.Windows.Forms.TextBox txtRSPW;
    private System.Windows.Forms.Label label8;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.TextBox txtSAPW;
  }
}

